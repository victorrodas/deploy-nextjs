import React from 'react';
import styled from 'styled-components';


const Example2  = () =>{
    return <MyComponent />
}


const MyComponent = styled.div`
    background-color:green;
    height:50px;
    width:50px;
`;

export default Example2;
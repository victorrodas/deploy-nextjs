import React from 'react';

import Example from '../components/home/example';

export default {
  title: 'Example/Docker',
  component: Example,
};

export const ExampleDocker = (args) => <Example />;


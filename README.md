# Deploy NextJS 
![variables](./img/pipelines.png)
## The Project has:
* NextJS
* Test Jest(Enzyme)
* Storybook
* Docker
* Deploy with Gitlab CI

## Installation Project
* Clone Project
```
    git clone git@gitlab.com:victorrodas/deploy-nextjs.git
```
* Go to folder clone
  
* *If you have Node >= 12*
* Install requirements
```
npm install
or
yarn install
```
* Run Project
```
yarn dev
```
* Go to http://localhost:3000/

* *If you don't have Node >= 12, the project have docker-compose*
```
docker-compose build
```
this created container with Node 12 for we work
*  For enter to container, we have Makefile
```
make frontend
or
docker-compose run --rm --service-ports frontend sh
```
* Run Project
```
yarn dev
```
* Go to http://localhost:3000/
## Run Tests
The project has setup for testing with Jest (Enzyme) to run the test:
```
yarn jest
```
## Run StoryBook
* Run command:
```
yarn storybook
```
* Go to http://localhost:6006/

## Run And Config Deploy
* All configuration for gitlab ci in file: .gitlab-ci.yml
* We configure the pipelines:
  * Install Requirements
  * Run Tests
  * Deploy Staging(Manual)(Heroku)
  * Deploy Produccion(Manual)
When you will upload project, the pipeline execute automatically, exception deploy stagin and deploy production are manual
### Configurate Staging for deploy Heroku
* Config you project Modify package.json, change start and add heroku-postbuild
```
"start": "next start -p $PORT",
"heroku-postbuild": "npm run build"
```
* Create Account in https://signup.heroku.com/
* Install Heroku CLI https://devcenter.heroku.com/articles/heroku-cli
* Go folder clone and:
* Create Account
```
heroku create deploy-nextjs-project
```
You change (deploy-nextjs-project)
* And Push 
```
git push heroku master
```
this return URL
* We need HEROKU_USER and HEROKU_API_KEY
```
heroku authorizations:create
heroku auth:token
```
![variables](./img/create_token.png)
heroku auth:token -> return HEROKU_API_KEY TOKEN 
HEROKU_USER -> is your email exception @..........
* GOOD!
* We need create variable in GITLAB CI 
* Go Settings -> CI/CD -> VARIBALES CLICK BUTTON EXPAND
![variables](./img/var.png)
* Good!.
- "There is still no production configuration, it is in process ..."

## Good Now you have Project with Continuous Deploy 🙂🙂🙂!!!.

- Linkedin: [Victor Rodas](https://www.linkedin.com/in/victorrodas/)
- Github: [VictorRancesCode](https://github.com/VictorRancesCode)
- Medium: [VictorDevCode](https://medium.com/@victordevcode) 
///import { render, screen } from "@testing-library/react";
import React from 'react';
import App from "../pages/";
import { shallow } from 'enzyme';


describe("App", () => {
  it("renders without crashing", () => {
    const wrapper = shallow(<App />);
    expect(wrapper.getElements()).toMatchSnapshot();
  });
});